import sys
import requests
import json
import os
from datetime import datetime


def load_data(data, filename):
    with open(filename) as file:
        items = json.load(file)
        for item in items:
            data[item['symbol']] = item


def date_is_latest(current, target):
    d1 = datetime.strptime(current, '%Y-%m-%d')
    d2 = datetime.strptime(target, '%Y-%m-%d')
    return d2 > d1


def get_market_data(date):
    data = {
        'item': "summary",
        'date': date
    }
    r = requests.post(url="https://market.capitalstake.com/rq", data=data)
    data = r.json()
    return data['data']['equities']


def update_data(data, equities, date_str):
    for symbol in equities:
        if symbol in data:
            if not date_is_latest(data[symbol]['last_updated'], date_str):
                print("{} is old date".format(date_str))
                continue

            data[symbol]['last_updated'] = date_str
            data[symbol]['high'] = max(data[symbol]['high'], equities[symbol]['high'])
            data[symbol]['low'] = min(data[symbol]['low'], equities[symbol]['low'])
            data[symbol]['close'] = equities[symbol]['close']
            data[symbol]['volume'] = sum([data[symbol]['volume'], equities[symbol]['volume']])
        else:
            data[symbol] = {
                'month': date_str[:-3],
                'last_updated': date_str,
                'symbol': symbol,
                'lmcp': equities[symbol]['ldcp'],
                'open': equities[symbol]['open'],
                'high': equities[symbol]['high'],
                'low' : equities[symbol]['low'],
                'close': equities[symbol]['close'],
                'volume': equities[symbol]['volume']
            }


def save_data(data, filename):
    arr = list(data.values())
    arr = sorted(arr, key=lambda k: k['volume'], reverse=True)

    with open(filename, 'w') as fp:
        json.dump(arr, fp, indent=4, sort_keys=True)


def run(arg):
    date_str = arg[0]
    year, mon, day = map(str, date_str.split("-"))

    filename = "{}-{}.txt".format(year, mon)
    data = {}
    if os.path.isfile(filename):
        load_data(data, filename)

    equities = get_market_data(date_str)
    update_data(data, equities, date_str)
    save_data(data, filename)


if __name__ == '__main__':
    run(sys.argv[1:])
