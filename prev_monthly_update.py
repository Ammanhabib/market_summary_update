import sys
import requests
import json
import os
import datetime as dt
from datetime import datetime
from dateutil.parser import parse


def compare_values(curr, old, key):
    if key == 'high':
        if curr > old:
            return curr
        else:
            return old

    elif key == 'low':
        if curr < old:
            return curr
        else:
            return old
    elif key == 'volume':
        old = old + curr
        return old


def check_date(loaded_file, date):
    print("checking date")
    year, mon, day = map(str, date.split("-"))

    l_date = loaded_file['last_updated']
    year_l, mon_l, day_l = map(str, l_date.split("-"))

    params_date = dt.datetime(int(year), int(mon), int(day))
    lmd = dt.datetime(int(year_l), int(mon_l), int(day_l))

    if params_date > lmd:
        print("update file")
        return True

    return False


def check_presence(loaded_file, symbol):
    for index, company in enumerate(loaded_file):
        if company['symbol'] == symbol:
            return True, company, index
    return False, None, None


def update_company(present, date, data_key, lfi):
    if present:
        updated_obj = {
            'month': data_key['month'],
            'last_updated': date,
            'symbol': data_key['symbol'],
            'lmcp': data_key['lmcp'],
            'open': data_key['open'],
            'high': compare_values(data_key['high'], lfi['high'], 'high'),
            'low': compare_values(data_key['low'], lfi['low'], 'low'),
            'close': data_key['close'],
            'volume': compare_values(data_key['volume'], lfi['volume'], 'volume')
        }
    else:
        updated_obj = {
            'month': data_key['month'],
            'last_updated': date,
            'symbol': data_key['symbol'],
            'lmcp': data_key['lmcp'],
            'open': data_key['open'],
            'high': (data_key['high']),
            'low': data_key['high'],
            'close': data_key['close'],
            'volume': data_key['volume']
        }
    return updated_obj


def update_file(loaded_file, date):
    check = check_date(loaded_file[0], date)
    updated_list = []
    year, mon, day = map(str, date.split("-"))

    if check is True:

        data_list = fetch_data(date, True)
        for data_key in data_list:
            symbol = data_key['symbol']
            present, lfi, index = check_presence(loaded_file, symbol)
            updated_obj = update_company(present, date, data_key, lfi)
            if present:
                loaded_file[index] = updated_obj
            else:
                loaded_file.append(updated_obj)



    else:
        print("File already updated no need to update")

    with open(mon + "-" + year + ".txt", 'w') as fp:
        json.dump(loaded_file, fp, indent=4, sort_keys=True)


def load_data(year, mon):
    with open(mon+"-"+year+".txt") as json_file:
        loaded_data = json.load(json_file)
    return loaded_data


def check_if_file_exists(year, mon):

    if os.path.isfile(mon+"-"+year+".txt"):
        print("file exists")
        return True
    return False


def get_post_req(date):
    date = dt.datetime.strptime(date, '%Y-%m-%d').strftime('%Y-%m-%d')

    data = {
        'item': "summary",
        'date': date

    }
    r = requests.post(url="https://market.capitalstake.com/rq", data=data)
    data_eq = r.json()
    equities = data_eq['data']
    equities = equities['equities']

    return equities


def fetch_data(date, in_func):

    equities = get_post_req(date)
    data_list = []
    year, mon, day = map(str, date.split("-"))
    for symbol in equities:
        obj = {
            'month': year+"-" + mon,
            'last_updated': '2019-04-01',
            'symbol': symbol,
            'lmcp': equities[symbol]['ldcp'],
            'open': equities[symbol]['open'],
            'high': equities[symbol]['high'],
            'low' : equities[symbol]['low'],
            'close': equities[symbol]['close'],
            'volume': equities[symbol]['volume']
        }
        data_list.append(obj)

    print(data_list)
    if in_func is False:
        with open(mon+"-"+year+".txt", 'w') as fp:
            json.dump(data_list, fp, indent=4, sort_keys=True)
    else:
        return data_list


def run(arg):
    date = arg[0]
    dt = parse(date)
    date_str = datetime.strftime(dt, '%Y-%m-%d')
    year, mon, day = map(str, date_str.split("-"))
    check = check_if_file_exists(year, mon)
    if check:
        loaded_data = load_data(year, mon)
        update_file(loaded_data, date)
    else:
        fetch_data(date, False)


if __name__ == '__main__':
    run(sys.argv[1:])
